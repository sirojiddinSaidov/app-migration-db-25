package uz.pdp.appdbmigration.model;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Post {

    @Id
    private Integer id;

    private String title;

    private String body;

    private boolean deleted;
}
