package uz.pdp.appdbmigration.repository;

import org.springframework.data.repository.ListCrudRepository;
import org.springframework.data.repository.ListPagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import uz.pdp.appdbmigration.model.Post;

@RepositoryRestResource
public interface PostRepository extends
        ListPagingAndSortingRepository<Post, Integer>,
        ListCrudRepository<Post, Integer> {
}
