package uz.pdp.appdbmigration.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import uz.pdp.appdbmigration.model.Comment;

@RepositoryRestResource
public interface CommentRepository extends CrudRepository<Comment, Integer> {
}
