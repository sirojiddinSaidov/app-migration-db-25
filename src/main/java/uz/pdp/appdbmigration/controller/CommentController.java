package uz.pdp.appdbmigration.controller;

import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import uz.pdp.appdbmigration.model.Comment;
import uz.pdp.appdbmigration.model.Post;
import uz.pdp.appdbmigration.repository.CommentRepository;
import uz.pdp.appdbmigration.repository.PostRepository;

@RestController
@RequestMapping("/comment")
@RequiredArgsConstructor
public class CommentController {


    private final CommentRepository commentRepository;

    @PostMapping
    public HttpEntity<?> add(@Valid @RequestBody Comment comment) {
        commentRepository.save(comment);
        return ResponseEntity.ok(comment);
    }
}
