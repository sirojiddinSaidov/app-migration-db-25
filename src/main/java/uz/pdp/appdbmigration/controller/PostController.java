package uz.pdp.appdbmigration.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import uz.pdp.appdbmigration.model.Post;
import uz.pdp.appdbmigration.repository.PostRepository;

@RestController
@RequestMapping("/post")
@RequiredArgsConstructor
public class PostController {


    private final PostRepository postRepository;

    @PostMapping
    public HttpEntity<?> add(@RequestBody Post post) {
        postRepository.save(post);
        return ResponseEntity.ok(post);
    }
}
