package uz.pdp.appdbmigration;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AppDbMigrationApplication {

    public static void main(String[] args) {
        SpringApplication.run(AppDbMigrationApplication.class, args);
    }

}
